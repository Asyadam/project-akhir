@extends('layout.master')

@section('judul')
HALAMAN update FILM
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label>judul</label>
    <input type="text" name="judul" value="{{$film->judul}}" class="form-control">
  </div>
  @error('judul')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror


  <div class="form-group">
    <label>Ringkasan</label>
    <textarea name="ringkasan" class="form-control" id="" cols="30" rows="10">{{ $film->ringkasan }}</textarea>
  </div>
  @error('ringkasan')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror


  <div class="form-group">
    <label>Tahun</label>
    <input type="number" name="tahun" value="{{ $film->tahun }}" class="form-control">
  </div>
  @error('tahun')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Poster</label>
    <input type="file" name="poster" class="form-control">
  </div>
  @error('poster')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

      
  <div class="form-group">
    <label>Genre</label>
    <select name="genre_id" class="form-control" id="">
        <option value="">--Pilih Genre--</option>
       
        @forelse ($genre as $item)
            @if ($item->id === $film->genre_id)
                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
            @else
                <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endif 
        @empty
            <option value="">Tidak ada Data Genre</option>
        @endforelse
    </select>
</div>
@error('genre_id')
<div class="alert alert-danger">{{ $message }}</div>
@enderror



  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
