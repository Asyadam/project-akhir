@extends('layout.master')

@section('judul')
HALAMAN LIST genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary btn-sm mb-3">tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">NO</th>
        <th scope="col">nama</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($genre as $key => $value)
       <tr>
        <td>{{ $key +1 }}</td>
        <td>{{ $value->nama }}</td>
     
        <td>
            
            <form action="/genre/{{ $value->id }}" method="POST">
                @csrf
                @method('DELETE')
                <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/genre/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </td>

       </tr>
    @empty
        <tr>
            <td>Tidak ada data</td>
        </tr>
    @endforelse
    </tbody>
  </table>
@endsection
