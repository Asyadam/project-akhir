@extends('layout.master')

@section('judul')
HALAMAN List Film
@endsection

@section('content')
@auth
    
<a href="/film/create" class="btn btn-primary btn-sm mb-4">Tambah Film</a>
@endauth
<div class="row">
    @forelse ($film as $item)
    <div class="col-md-4 mb-4">
        <div class="card h-100 d-flex flex-column">
            <img src="{{ asset('image/' . $item->poster) }}" class="card-img-top" alt="{{ $item->judul }}">
            <div class="card-body d-flex flex-column">
                <h2>{{ $item->judul }}</h2>
                <span class="badge badge-info">{{ $item->genre->nama }}</span>
                <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                <p class="card-text">{{ $item->tahun }}</p>
                <div class="mt-auto">
                    <a href="/film/{{ $item->id }}" class="btn btn-warning w-100 mb-2">Detail</a>
                 
                    @auth
                        
                   
                    <a href="/film/{{ $item->id }}/edit" class="btn btn-info w-100 mb-2">Edit</a>
                    <form action="/film/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-block" value="Delete">
                    </form>
                    @endauth
                </div>
            </div>
        </div>
    </div>
    @empty
    <h2>tidak ada Film</h2>
    @endforelse
</div>
@endsection
