@extends('layout.master')

@section('judul')
HALAMAN Detail FILM
@endsection

@section('content')

<div class="card h-100 d-flex flex-column">
    <div class="d-flex justify-content-center align-items-center" style="height: 400px;">
        <img src="{{ asset('image/' . $film->poster) }}" class="card-img-top custom-img" alt="{{ $film->judul }}" style="max-width: 100%; max-height: 100%; object-fit: contain;">
    </div>
    <div class="card-body d-flex flex-column">
        <h2>{{ $film->judul }}</h2>
        <p class="card-text">{{ $film->tahun }}</p>
        <p class="card-text">{{ $film->ringkasan }}</p>

        <hr>
        <h4>Kumpulan kritik dan saran</h4>
    @forelse ($film->Kritik as $item)
    <div class="card">
        <div class="card-header">
          {{ $item->user->name }}
        </div>
        <div class="card-body">
            <h5 class="card-title">Poin: {{ $item->point }}</h5> <!-- Menampilkan poin yang diberikan -->
          <p class="card-text">{{ $item->content}}</p>
         
        </div>
      </div>
        
    @empty
        <h4>tidak ada kritik di film ini</h4>
    @endforelse

        @auth
            
    
        <hr>
        <h4>beri kritik dan saran</h4>
        <form action="/kritik/{{ $film->id }}" method="POST" class="mb-5">
            @csrf
            <textarea name="content" cols="30" rows="10" class="form-control" placeholder="Isi kritik"></textarea> <br>
            <input type="number" name="point" class="form-control" placeholder="Masukkan point (1-10)" min="1" max="10"> <br>
            <input type="submit" value="Beri kritik dan saran" class="btn btn-sm btn-block btn-primary">
        </form>
        @endauth
        



        <a href="/film" class="btn btn-secondary mt-auto">Kembali</a>
    </div>
</div>

@endsection

