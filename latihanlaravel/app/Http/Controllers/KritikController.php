<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Kritik;


class KritikController extends Controller
{
    public function store($film_id, Request $request)
    {
        // Validasi input dari request
        $request->validate([
            'content' => 'required',
            'point' => 'required|integer|min:1|max:10' // Asumsi poin adalah nilai integer antara 1 dan 10
        ]);

        // Buat entri baru di tabel kritik
        Kritik::create([
            'user_id' => Auth::id(), // Mendapatkan ID pengguna yang sedang login
            'film_id' => $film_id, // ID film yang diterima sebagai parameter
            'content' => $request->input('content'), // Konten kritik dari input
            'point' => $request->input('point') // Poin dari input
        ]);

        // Redirect kembali ke halaman film dengan ID yang diberikan
        return redirect ('/film/'.$film_id);
    }
}
