<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\KritikController;

use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);    


Route::get('/data-table', function(){
return view('page.table');
});

Route::get('/table', function(){
    return view('page.table2');
    });

    

    //CRUD CATEGORY
    //CREATE
   Route::get('/cast/create',[CastController::class, 'create']);
   //untuk ke database
   Route::post('/cast', [CastController::class,'store']);


   //read
   Route::get('/cast',[CastController::class, 'index']);

   //detail
   Route::get('/cast/{cast_id}',[CastController::class, 'show']);


   //update
   Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
   //update ke database
   Route::put('/cast/{cast_id}',[CastController::class, 'update']);

   //delete
   Route::delete('/cast/{cast_id}',[CastController::class, 'destroy']);



       //CRUD GENRE
    //CREATE

    Route::middleware(['auth'])->group(function () {
            // Uses first & second middleware...
            Route::get('/genre/create',[GenreController::class, 'create']);
            //untuk ke database
            Route::post('/genre', [GenreController::class,'store']);
         
         
            //read
            Route::get('/genre',[GenreController::class, 'index']);
         
            //detail
            Route::get('/genre/{genre_id}',[GenreController::class, 'show']);
         
         
            //update
            Route::get('/genre/{genre_id}/edit',[GenreController::class, 'edit']);
            //update ke database
            Route::put('/genre/{genre_id}',[GenreController::class, 'update']);
         
            //delete
            Route::delete('/genre/{genre_id}',[GenreController::class, 'destroy']);


          Route::post('/kritik/{film_id}', [KritikController::class,'store']);
          
        });
     
      
  
   
 

    //CRUD film
    //form  film kategori
    Route::resource('film', FilmController::class);
    
Auth::routes();


