@extends('layout.master')

@section('judul')
HALAMAN detail Genre
@endsection

@section('content')
<h1>Genre {{ $genre->nama }}</h1>

<div class="row">
@forelse ($genre->listFilm as $item)
<div class="col-md-4 mb-4">
    <div class="card h-100 d-flex flex-column">
        <img src="{{ asset('image/' . $item->poster) }}" class="card-img-top" alt="{{ $item->judul }}">
        <div class="card-body d-flex flex-column">
            <h2>{{ $item->judul }}</h2>
            <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
            <p class="card-text">{{ $item->tahun }}</p>
            <div class="mt-auto">
                <a href="/film/{{ $item->id }}" class="btn btn-warning w-100 mb-2">Detail</a>
             
            </div>
        </div>
    </div>
</div>
@empty
    <h4>Tidak ada film di genre ini</h4>
@endforelse
</div>

<a href="/genre" class="btn btn-danger btn-sm"> kembali</a>
@endsection
